var Device = require('zetta-device');
var util = require('util');

// Name defaults to 'relay' if not provided
var Relay = module.exports = function(name) {
  Device.call(this);
  this.name = name || 'relay';

  this.onOff = 'Off';
}

util.inherits(Relay, Device);

Relay.prototype.init = function(config) {
  config
    .name(this.name)
    .type('light')
    .monitor('onOff')
    .state('ready')
    .when('ready', {allow:['On','Off','Toggle']})
    .map('On', this.turnOn)
    .map('Off', this.turnOff)
    .map('Toggle', this.toggle);
}

Relay.prototype.turnOn = function(cb) {
  this.onOff = 'On';
  cb();
}

Relay.prototype.turnOff = function(cb) {
  this.onOff = 'Off';
  cb();
}

Relay.prototype.toggle = function(cb) {
  if(this.onOff == 'Off') {
    this.onOff = 'On';
  } else {
    this.onOff = 'Off';
  }
  cb();
}